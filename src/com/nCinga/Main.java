package com.nCinga;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Account account1 = new Account(1111, 5000, new Person("Yasas"));

        Thread thread1 = new Thread(new Transaction(account1, 100, TransType.DEPOSIT));
        Thread thread2 = new Thread(new Transaction(account1, 200, TransType.WITHDRAW));
        Thread thread3 = new Thread(new Transaction(account1, 300, TransType.DEPOSIT));
        Thread thread4 = new Thread(new Transaction(account1, 100, TransType.DEPOSIT));

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();

        thread1.join();
        thread2.join();
        thread3.join();
        thread4.join();

        account1.displayBalance();
    }
}
