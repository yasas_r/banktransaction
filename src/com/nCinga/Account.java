package com.nCinga;

public class Account {

    private final int accountNo;
    private int balance;
    private Person person;

    public Account(int accountNo, int balance, Person person) {
        this.accountNo = accountNo;
        this.balance = balance;
        this.person = person;
    }

    public Account(int accountNo, Person person) {
        this.accountNo = accountNo;
        this.person = person;
        this.balance = 1000;
    }

    public void displayBalance() {
        System.out.println(this.getPerson().getName() + " : Your balance is " + this.getBalance());
    }

    public synchronized void deposit(int amount) {
        this.setBalance(this.balance + amount);
        System.out.print(this.getPerson().getName());
        System.out.println(" : Deposit " + amount + " is completed...Current balance is " + this.getBalance());
    }

    public synchronized void withdraw(int amount) {
        if (this.balance >= amount) {
            this.setBalance(this.balance - amount);
            System.out.print(this.getPerson().getName());
            System.out.println(" : Withdraw " + amount + " is completed...Current balance is " + this.getBalance());
        } else {
            System.out.print("Doesn't have enough account balance for withdraw ");
            System.out.println("Your balance is " + this.getBalance());
        }
    }

    public int getBalance() {
        return balance;
    }

    public int getAccountNo() {
        return accountNo;
    }

    public Person getPerson() {
        return person;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
