package com.nCinga;

public class Transaction implements Runnable {

    private final Account account;
    private final int amount;
    private final TransType transType;

    public Transaction(Account account, int amount, TransType transType) {
        this.account = account;
        this.amount = amount;
        this.transType = transType;
    }

    private void transDeposit(int amount) {
        this.account.deposit(amount);
    }

    private void transWithdraw(int amount) {
        this.account.withdraw(amount);
    }

    @Override
    public void run() {
        if (transType == TransType.DEPOSIT) {
            transDeposit(this.amount);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if (transType == TransType.WITHDRAW) {
            transWithdraw(this.amount);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Account getAccount() {
        return account;
    }

    public int getAmount() {
        return amount;
    }

    public TransType getTransType() {
        return transType;
    }
}
